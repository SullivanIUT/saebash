 #!/bin/bash

 # DEMARET Sullivan Groupe: 1A3B
 # Ce script permet d'installer les extensions nécessaires en BUT Informatique.

intro=1
while [ $intro = 1 ]
do
	echo -e "Bienvenue dans le programme d'installation  d'un poste Linux.\Nous allons les prérequis et mettre à jour les paquets, êtes-vous d'accord ? (Y/N) : "
	read debut
	if [ $debut = "Y" ] || [ $debut = "y" ]
	then
		intro=2
	elif [ $debut = "N" ] || [ $debut = "n" ]
	then
		intro=0
		echo "Vous quittez l'installation"
	else
		echo "Réponse non valide, réessayez avec une réponse valide"
	fi
done
if [ $intro = 2 ]
then
	sudo apt update
	sudo apt upgrade
	sudo apt install software-properties-common apt-transport-https wget
	wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add - 
	sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
	sudo apt update
	install=0
	while [ $install = 0 ]
	do
		echo -e "Que souhaitez-vous installer ?\n-VsCode\n-Java\n-Git (pour cloner et déposer des fichiers dans git)\n-Python\n-Docker\n-All (Pour tout installer)\nOu \"Quitter\" pour quitter"
		read rep
		if [ $rep = "VsCode" ] || [ $rep = "All" ]
		then
			echo "Installation de VsCode et de ses extensions en cours..."
			apt install code
			code --install-extension ms-python.python
			code --install-extension njpwerner.autodocstring
			code --install-extension ms-ceintl.vscode-language-pack-fr
			code --install-extension gitlab.gitlab-workflow
			code --install-extension cameron.vscode-pytest
			code --install-extension vscjava.vscode-java-pack
			code --install-extension vscode-icons-team.vscode-icons
			echo "Installation de VsCode terminée, vous pouvez désormais le lancer avec la commande code."
		fi
		if [ $rep = "Java" ] || [ $rep = "All" ]
		then
			echo "Installation de Java en cours..."
			sudo apt install default-jdk
			echo "Installation de Java terminée, exécution d'un \"Hello World\""
			echo -e "public class Hello {\n\n    public static void main(String[] args){\n\n  System.out.println(\" Hello World ! \") ;\n\n  }\n\n}" > Hello.java
			javac Hello.java
			java Hello
		fi
		if [ $rep = "Git" ] || [ $rep = "All" ]
		then
			echo "Installation de Git en cours..."
			sudo apt-get install git
			echo "Installation de Git terminée"
		fi
		if [ $rep = "Python" ] || [ $rep = "All" ]
		then
			echo "Création et exécution d'un fichier python"
			echo -e "#!/bin/python3\nprint (\"Hello World !\")" > algo.py
			chmod u+x algo.py
			./algo.py
		fi
		if [ $rep = "Docker" ] || [ $rep = "All" ]
		then
			echo "Installation de Docker en cours..."
			sudo apt update
			sudo apt install apt-transport-https ca-certificates curl software-properties-common
			sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add –
			sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
			sudo apt update
			sudo apt install docker-ce
			sudo apt install docker.io
		fi
		if [ $rep = "Quitter" ]
		then
			echo "Vous avez quitter le script"
			install=1
		fi
		if [ $rep = "All" ]
		then
			echo "Tous les logiciels ont été installés, appréciez votre nouveau postee de travail."
			install=1
		fi
	done
fi