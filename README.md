# SAE 1.03 Installer un système

**DEMARET Sullivan Groupe:1A3B**

lien gitlab: https://gitlab.com/SullivanIUT/saebash

Pour reproduire cette installation Ubuntu, il est nécessaire d'avoir certaines bases en informatique notamment en Bash. En effet, pour les dernières étapes cela nécessitent quelques connaissances dans l'utilisation du terminal.


# Installer et Configurer Ubuntu

Ces étapes sont destinées à une installation boot entièrement dédié à Ubuntu.
Cette installation est concerné par les personnes ayant un pc Windows 10. J'ai fait ce choix car pour personnellement je n'ai pas besoin de Windows, Ubuntu me suffit amplement pour le travail à effectuer en BUT Informatique.

## Étape 1: Prérequis
- Premièrement avant toute manipulation sur votre machine, veillez à faire une sauvegarde de vos fichiers.
	>Vous pouvez les sauvegarder sur une clé usb ou un disque dur.
- Ensuite il vous faut une clé bootable Ubuntu.Pour cela, vous pouvez installer l'image ISO via ce [lien](https://lecrabeinfo.net/telecharger/ubuntu-20-04-lts), ceci est la version 20.04 de Ubuntu.
	>Pour pouvoir le transférer sur la clé USB, il est nécessaire que celle-ci est un espace de stockage supérieur à 2.5 Go.



## Étape 2: Installation de Ubuntu

Pour commencer l'installation, **insérer** votre clé bootable sur l'un des ports de votre pc.

Ensuite allez dans les **paramètres** de Windows situés en bas à gauche de votre bureau puis cliquer sur **Mise à jour et sécurité**. Ensuite, cliquer sur l'onglet **Récupération** puis sur **Redémarrer maintenant**.

Choisissez l'option **Utiliser un périphérique** ensuite cliquez sur votre périphérique USB. L'ordinateur va redémarrer sur votre clé. Une fois cela fait,  un menu apparaît. Il permet de choisir le mode de lancement d'Ubuntu, à gauche vous pouvez sélectionner votre langue et à droite votre mode de lancement Essayer ubuntu permet de tester le système d'exploitation sans apporter de modification à votre pc. Il y a ensuite la deuxième **Installer Ubuntu**, cliquez sur celui-ci. Sélectionner ensuite votre clavier, et appuyez sur continuer. Vous pouvez après ceci sélectionner des mise à jour et des pilotes pendant l'installation,cliquez sur continuer et **Effacer le disque et installer Ubuntu**.
>RAPPEL : N'oubliez pas de faire une sauvegarder de vos fichiers sinon ils ne seront pas récupérables.

Ensuite cliquez sur **Installer maintenant**, une demande de confirmation est nécessaire cliquer sur **Continuer**.
Faites les dernières configurations puis terminer en cliquant sur **Redémarrer maintenant**

Et voilà vous avez votre système d'exploitation Ubuntu ^^


## Étape 3: Configuration d'Ubuntu

Appuyez sur la touche **Window** + **T** sur votre clavier. Un terminale s'ouvre, pour vous simplifier la tâche vous trouvez [ici](https://gitlab.com/SullivanIUT/saebash/-/blob/main/script_Sullivan.sh) un script permettant de télécharger les extensions nécessaires. Une fois celui-ci télécharger vérifier que vous disposer des droits d’exécution. En effectuant la commande ls -l scrpit.sh en étant dans le répertoire de là où se situe le script.Vous devriez obtenir ceci:
- -rw?- - - - - - :  Si à la place du point d'interrogation il y a un x cela signifie que vous avez les droits d’exécution dans le cas contraire effectuer la commande chmod 700 script.sh

Une fois que le fichier est bien exécutable lancez le en effectuant la commande ./script.sh et suivez les indications sur ce que vous souhaitez installer. Puis quittez le script une fois les installations finies et votre pc est prêt à l'utilisation !